const path = require("path");
const hbs = require("hbs");
const express = require("express");
const app = express();
const port=process.env.PORT ||3000
const geoCode = require("./utils/geocode");
const foreCast = require("./utils/forecast");
// console.log(__dirname); for going till directory
// console.log(__filename);  for going till file
// console.log(path.join(__dirname,'../public'));  path for joining

//setup handlebars engine and views location
app.set("view engine", "hbs");
app.set("views", path.join(__dirname, "../Templates/views"));
hbs.registerPartials(path.join(__dirname, "../Templates/partials"));
//setup static directory to serve static files
app.use(express.static(path.join(__dirname, "../public"))); //for serving static files
app.get("", (req, res) => {
  res.render("index", { title: "Nirali", name: "Nirali Vaghela" });
});
app.get("/products", (req, res) => {
  if (!req.query.search) {
    return res.send({ error: "You must provide a search term" });
  }

  res.send({ products: [] });
});
app.get("/weather", (req, res) => {
  if (!req.query.address) {
    return res.send({ error: "You must provide an address" });
  }
  geoCode(req.query.address, (error, { latitude, longitude, location }={}) => {
    if (error) {
      return res.send({ error });
    }

    foreCast(longitude, latitude, (error, forecastData) => {
      if (error) {
        return res.send({ error });
      }
      res.send({
        forecast: forecastData,
        location,
        address: req.query.address,
      });
    });
  });
});
app.get("/about", (req, res) => {
  res.render("about", { title: "About me?", name: "Nirali Vaghela" });
}),
  app.get("/help", (req, res) => {
    res.render("help", { title: "Help", name: "Nirali Vaghela" });
  }),
  app.get("/help/*", (req, res) => {
    res.render("404", {
      title: "404",
      name: "Nirali Vaghela",
      errorMessage: "Help article not found.",
    });
  }),
  app.get("*", (req, res) => {
    res.render("error", {
      title: "My 404 Page",
      name: "Nirali Vaghela",
      errrMessage: "Page not found",
    });
  });

// app.get("/help", (req, res) => {
//   res.send("Help me");
// });
// app.get("/weather", (req, res) => {
//   res.send("<h1>Weather</h1>");
// });
// app.get("/about", (req, res) => {
//   res.send({ name: "NV", age: 20 });
// });
app.listen(port), () => console.log("server started on port 3000");
