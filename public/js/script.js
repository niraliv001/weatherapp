const form = document.querySelector("form");
const inp = document.querySelector("input");
const meOne = document.querySelector("#message-1");
const meTwo = document.querySelector("#message-2");
form.addEventListener("submit", (e) => {
  e.preventDefault();
  const location = inp.value;
  // console.log(location);
  meOne.textContent = "Loading...";
  meTwo.textContent = "";
  fetch("/weather?address=" + location).then((response) =>
    response.json().then((data) => {
      if (data.error) {
        meOne.textContent = data.error;
      } else {
        let pElem = document.createElement("p");
        pElem.innerHTML = "Location: ";
        meOne.textContent = data.location;
        meTwo.textContent = data.forecast;
        meOne.prepend(pElem);
        // console.log(data.location);
        // console.log(data);
      }
    })
  );
});
